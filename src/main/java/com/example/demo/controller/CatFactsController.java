package com.example.demo.controller;


import com.example.demo.model.CatFact;
import com.example.demo.service.CatFactServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class CatFactsController {

    @Autowired
    private CatFactServiceImpl service;

    @RequestMapping(value = "/getCatFacts",produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.GET)
    public List<CatFact> getCatFacts() throws JsonProcessingException {
        return  service.getTenFacts();
    }

}
