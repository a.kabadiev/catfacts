package com.example.demo.model;

public class CatFact {

    private String _id;
    private String text;
    private String author;

    public String get_id() {
        return _id;
    }

    public String getText() {
        return text;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "CatFact{" +
                "_id='" + _id + '\'' +
                ", text='" + text + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
