package com.example.demo.service;

import com.example.demo.model.CatFact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CatFactServiceImpl {

    public static final String FIELD_TEXT = "text";
    public static final String FIELD_NAME = "name";
    public static final String AUTHOR_FIRST_NAME = "first";
    public static final int NUMBER_OF_FACTS = 10;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${url}")
    private String url;
    @Value("${api.key}")
    private String apiKey;
    @Value("${api.host}")
    private String host;

    public List<CatFact> getTenFacts() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("x-rapidapi-host", host);
        headers.set("x-rapidapi-key", apiKey);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode data = root.path("all");

        List<CatFact> facts = new ArrayList<>();

        for (int nodeNumber = 0; nodeNumber < NUMBER_OF_FACTS; nodeNumber++) {
            JsonNode node = data.get(nodeNumber);
            CatFact fact = new CatFact();
            fact.set_id(node.get("_id").textValue());
            fact.setAuthor(node.get("user").get(FIELD_NAME).get(AUTHOR_FIRST_NAME).textValue());
            fact.setText(node.get(FIELD_TEXT).textValue());

            facts.add(fact);
        }

        return facts;
    }
}
